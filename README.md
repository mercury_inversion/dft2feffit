# dft2feffit

## presentation

The Python code of the dft2feffit software calculates ....

Authors: Romain Brossier (romain.brossier@univ-grenoble-alpes.fr) Alain Manceau (alain.manceau@ens-lyon.fr)

This code is associated with the article ....

---------------------------------
## inputs

The code requires one general input file, a dataset in .txt format + feff or 2 columns format data to be used in teh fitting inversion process. The input files and datasets used xxx are provided in the Git repository.

### the general structure of the txt input file is as follows


file name fo fit

n (integer) number of path

k (float) k weighting of the misfit function

kmin kmax (floats), kmin and kmax values to consider in the misfit

S_0^2 (float) S_0 squared to consider

choice to invert also for DeltaE (integer) (1) or not (0)

DeltaE (float) for fixed value, or DeltaEmin(float) DeltaEmax(float) DeltaDeltaE(float) for exploration

index (int) Path name (char) Distance (float) file name (char) opt (integer) init (float) min (float) max (float) cor (integer)


---------------------------------------
## data examples
the directory input provides :

---------------------------------------

## running examples

1. To reproduce xx, launch `python3 dft2feffit.py` and specify input.txt as input file 
